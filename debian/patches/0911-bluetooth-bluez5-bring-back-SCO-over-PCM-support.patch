From: Simon Fels <simon.fels@canonical.com>
Date: Sun, 1 Nov 2015 16:38:39 +0100
Subject: [PATCH 502/507] bluetooth: bluez5: bring back SCO over PCM support

[ratchanan@ubports.com:
- port to Pulseaudio 13.99.1
  - follow pa_{sink,source}_get_state removal
- make transport_state_to_string available for use by the module code
  for logging purpose.
- fix double-free in module-bluez5-discover. This was fixed differently
  in the Xenial patch, but matchs how upstream fix a similar problem
  in module-bluez5-card.
- Edit the patch to remove whitespace-only hunk in the later patch.
]

[ratchanan@ubports.com:
- port to Pulseaudio 16.1.
- remove unnecessary holding of pa_modargs. In case of module-bluez5-
  discover the required fields are kept inside struct userdata directly.
  In case of module-bluez5-device it was never required to begin with.
- follow changes in Bluetooth module's profile constants.
- don't set Bluetooth module's volume callbacks on SCO-over-PCM source
  & sink.
- don't call stream_died_cb over and over.
]
---
 src/modules/bluetooth/bluez5-util.c            |   4 +-
 src/modules/bluetooth/bluez5-util.h            |   1 +
 src/modules/bluetooth/module-bluez5-device.c   | 470 +++++++++++++++++++------
 src/modules/bluetooth/module-bluez5-discover.c |  23 ++
 4 files changed, 393 insertions(+), 105 deletions(-)

diff --git a/src/modules/bluetooth/bluez5-util.c b/src/modules/bluetooth/bluez5-util.c
index 596f008..badc097 100644
--- a/src/modules/bluetooth/bluez5-util.c
+++ b/src/modules/bluetooth/bluez5-util.c
@@ -241,7 +241,7 @@ void pa_bluetooth_transport_reconfigure(pa_bluetooth_transport *t, const pa_bt_c
     t->last_read_size = 0;
 }
 
-static const char *transport_state_to_string(pa_bluetooth_transport_state_t state) {
+const char *pa_bluetooth_transport_state_to_string(pa_bluetooth_transport_state_t state) {
     switch(state) {
         case PA_BLUETOOTH_TRANSPORT_STATE_DISCONNECTED:
             return "disconnected";
@@ -520,7 +520,7 @@ void pa_bluetooth_transport_set_state(pa_bluetooth_transport *t, pa_bluetooth_tr
     old_any_connected = pa_bluetooth_device_any_transport_connected(t->device);
 
     pa_log_debug("Transport %s state: %s -> %s",
-                 t->path, transport_state_to_string(t->state), transport_state_to_string(state));
+                 t->path, pa_bluetooth_transport_state_to_string(t->state), pa_bluetooth_transport_state_to_string(state));
 
     t->state = state;
 
diff --git a/src/modules/bluetooth/bluez5-util.h b/src/modules/bluetooth/bluez5-util.h
index 86eb630..18a2433 100644
--- a/src/modules/bluetooth/bluez5-util.h
+++ b/src/modules/bluetooth/bluez5-util.h
@@ -227,6 +227,7 @@ pa_bluetooth_device* pa_bluetooth_discovery_get_device_by_address(pa_bluetooth_d
 pa_hook* pa_bluetooth_discovery_hook(pa_bluetooth_discovery *y, pa_bluetooth_hook_t hook);
 
 const char *pa_bluetooth_profile_to_string(pa_bluetooth_profile_t profile);
+const char *pa_bluetooth_transport_state_to_string(pa_bluetooth_transport_state_t state);
 bool pa_bluetooth_profile_should_attenuate_volume(pa_bluetooth_profile_t profile);
 bool pa_bluetooth_profile_is_a2dp(pa_bluetooth_profile_t profile);
 
diff --git a/src/modules/bluetooth/module-bluez5-device.c b/src/modules/bluetooth/module-bluez5-device.c
index a30372e..49f21e4 100644
--- a/src/modules/bluetooth/module-bluez5-device.c
+++ b/src/modules/bluetooth/module-bluez5-device.c
@@ -32,6 +32,7 @@
 #include <pulse/utf8.h>
 #include <pulse/util.h>
 
+#include <pulsecore/core.h>
 #include <pulsecore/core-error.h>
 #include <pulsecore/core-rtclock.h>
 #include <pulsecore/core-util.h>
@@ -53,6 +54,9 @@
 #include <pulsecore/time-smoother.h>
 #endif
 
+#include <pulsecore/namereg.h>
+#include <pulse/mainloop-api.h>
+
 #include "a2dp-codecs.h"
 #include "a2dp-codec-util.h"
 #include "bluez5-util.h"
@@ -62,10 +66,12 @@ PA_MODULE_DESCRIPTION("BlueZ 5 Bluetooth audio sink and source");
 PA_MODULE_VERSION(PACKAGE_VERSION);
 PA_MODULE_LOAD_ONCE(false);
 PA_MODULE_USAGE(
-    "path=<device object path>"
-    "autodetect_mtu=<boolean>"
+    "path=<device object path> "
+    "autodetect_mtu=<boolean> "
     "output_rate_refresh_interval_ms=<interval between attempts to improve output rate in milliseconds>"
     "avrcp_absolute_volume=<synchronize volume with peer, true by default>"
+    "sco_sink=<name of sink> "
+    "sco_source=<name of source> "
 );
 
 #define FIXED_LATENCY_PLAYBACK_A2DP (25 * PA_USEC_PER_MSEC)
@@ -73,11 +79,20 @@ PA_MODULE_USAGE(
 #define FIXED_LATENCY_RECORD_A2DP   (25 * PA_USEC_PER_MSEC)
 #define FIXED_LATENCY_RECORD_SCO    (25 * PA_USEC_PER_MSEC)
 
+#define USE_SCO_OVER_PCM(u) \
+    ((u->profile == PA_BLUETOOTH_PROFILE_HSP_HS || \
+        u->profile == PA_BLUETOOTH_PROFILE_HSP_AG || \
+        u->profile == PA_BLUETOOTH_PROFILE_HFP_HF || \
+        u->profile == PA_BLUETOOTH_PROFILE_HFP_AG) && \
+     (u->hsp.sco_sink && u->hsp.sco_source))
+
 static const char* const valid_modargs[] = {
     "path",
     "autodetect_mtu",
     "output_rate_refresh_interval_ms",
     "avrcp_absolute_volume",
+    "sco_sink",
+    "sco_source",
     NULL
 };
 
@@ -103,6 +118,13 @@ typedef struct bluetooth_msg {
 PA_DEFINE_PRIVATE_CLASS(bluetooth_msg, pa_msgobject);
 #define BLUETOOTH_MSG(o) (bluetooth_msg_cast(o))
 
+struct hsp_info {
+    pa_sink *sco_sink;
+    void (*sco_sink_set_volume)(pa_sink *s);
+    pa_source *sco_source;
+    void (*sco_source_set_volume)(pa_source *s);
+};
+
 struct userdata {
     pa_module *module;
     pa_core *core;
@@ -116,6 +138,9 @@ struct userdata {
     pa_hook_slot *sink_volume_changed_slot;
     pa_hook_slot *source_volume_changed_slot;
 
+    pa_hook_slot *sink_state_changed_slot;
+    pa_hook_slot *source_state_changed_slot;
+
     pa_bluetooth_discovery *discovery;
     pa_bluetooth_device *device;
     pa_bluetooth_transport *transport;
@@ -166,6 +191,11 @@ struct userdata {
     size_t decoder_buffer_size;                  /* Size of the buffer */
 
     bool message_handler_registered;
+
+    struct hsp_info hsp;
+
+    bool transport_acquire_pending;
+    pa_io_event *stream_event;
 };
 
 typedef enum pa_bluetooth_form_factor {
@@ -599,6 +629,11 @@ static void teardown_stream(struct userdata *u) {
         u->rtpoll_item = NULL;
     }
 
+    if (u->stream_event) {
+        u->core->mainloop->io_free(u->stream_event);
+        u->stream_event = NULL;
+    }
+
     if (u->stream_fd >= 0) {
         pa_close(u->stream_fd);
         u->stream_fd = -1;
@@ -625,14 +660,23 @@ static void teardown_stream(struct userdata *u) {
 static int transport_acquire(struct userdata *u, bool optional) {
     pa_assert(u->transport);
 
-    if (u->transport_acquired)
+    if (u->transport_acquire_pending)
+        return -1;
+
+    if (u->transport_acquired) {
+        pa_log_debug("Transport already acquired");
         return 0;
+    }
+
+    u->transport_acquire_pending = true;
 
     pa_log_debug("Acquiring transport %s", u->transport->path);
 
     u->stream_fd = u->transport->acquire(u->transport, optional, &u->read_link_mtu, &u->write_link_mtu);
-    if (u->stream_fd < 0)
+    if (u->stream_fd < 0) {
+        u->transport_acquire_pending = false;
         return u->stream_fd;
+    }
 
     /* transport_acquired must be set before calling
      * pa_bluetooth_transport_set_state() */
@@ -646,6 +690,8 @@ static int transport_acquire(struct userdata *u, bool optional) {
             pa_bluetooth_transport_set_state(u->transport, PA_BLUETOOTH_TRANSPORT_STATE_PLAYING);
     }
 
+    u->transport_acquire_pending = false;
+
     return 0;
 }
 
@@ -690,6 +736,10 @@ static void handle_sink_block_size_change(struct userdata *u) {
 
 /* Run from I/O thread */
 static void transport_config_mtu(struct userdata *u) {
+
+    pa_log_debug("Configuring MTU for transport of profile %s",
+                 pa_bluetooth_profile_to_string(u->profile));
+
     pa_assert(u->bt_codec);
 
     if (u->encoder_info) {
@@ -710,6 +760,9 @@ static void transport_config_mtu(struct userdata *u) {
         }
     }
 
+    if (USE_SCO_OVER_PCM(u))
+        return;
+
     if (u->sink)
         handle_sink_block_size_change(u);
 
@@ -720,7 +773,7 @@ static void transport_config_mtu(struct userdata *u) {
                                                   pa_bytes_to_usec(u->read_block_size, &u->decoder_sample_spec));
 }
 
-/* Run from I/O thread */
+/* Run from I/O thread except in SCO over PCM */
 static int setup_stream(struct userdata *u) {
     struct pollfd *pollfd;
     int one;
@@ -1011,56 +1064,61 @@ static int add_source(struct userdata *u) {
 
     pa_assert(u->transport);
 
-    pa_source_new_data_init(&data);
-    data.module = u->module;
-    data.card = u->card;
-    data.driver = __FILE__;
-    data.name = pa_sprintf_malloc("bluez_source.%s.%s", u->device->address, pa_bluetooth_profile_to_string(u->profile));
-    data.namereg_fail = false;
-    pa_proplist_sets(data.proplist, "bluetooth.protocol", pa_bluetooth_profile_to_string(u->profile));
-    if (u->bt_codec)
-        pa_proplist_sets(data.proplist, PA_PROP_BLUETOOTH_CODEC, u->bt_codec->name);
-    pa_source_new_data_set_sample_spec(&data, &u->decoder_sample_spec);
-    if (u->profile == PA_BLUETOOTH_PROFILE_HSP_HS
-        || u->profile == PA_BLUETOOTH_PROFILE_HFP_HF)
-        pa_proplist_sets(data.proplist, PA_PROP_DEVICE_INTENDED_ROLES, "phone");
-
-    connect_ports(u, &data, PA_DIRECTION_INPUT);
-
-    if (!u->transport_acquired)
-        switch (u->profile) {
-            case PA_BLUETOOTH_PROFILE_A2DP_SOURCE:
-            case PA_BLUETOOTH_PROFILE_HFP_AG:
-            case PA_BLUETOOTH_PROFILE_HSP_AG:
-                data.suspend_cause = PA_SUSPEND_USER;
-                break;
-            case PA_BLUETOOTH_PROFILE_HSP_HS:
-            case PA_BLUETOOTH_PROFILE_HFP_HF:
-                /* u->stream_fd contains the error returned by the last transport_acquire()
-                 * EAGAIN means we are waiting for a NewConnection signal */
-                if (u->stream_fd == -EAGAIN)
+    if (USE_SCO_OVER_PCM(u)) {
+        u->source = u->hsp.sco_source;
+        pa_proplist_sets(u->source->proplist, "bluetooth.protocol", pa_bluetooth_profile_to_string(u->profile));
+    } else {
+        pa_source_new_data_init(&data);
+        data.module = u->module;
+        data.card = u->card;
+        data.driver = __FILE__;
+        data.name = pa_sprintf_malloc("bluez_source.%s.%s", u->device->address, pa_bluetooth_profile_to_string(u->profile));
+        data.namereg_fail = false;
+        pa_proplist_sets(data.proplist, "bluetooth.protocol", pa_bluetooth_profile_to_string(u->profile));
+        if (u->bt_codec)
+            pa_proplist_sets(data.proplist, PA_PROP_BLUETOOTH_CODEC, u->bt_codec->name);
+        pa_source_new_data_set_sample_spec(&data, &u->decoder_sample_spec);
+        if (u->profile == PA_BLUETOOTH_PROFILE_HSP_HS
+            || u->profile == PA_BLUETOOTH_PROFILE_HFP_HF)
+            pa_proplist_sets(data.proplist, PA_PROP_DEVICE_INTENDED_ROLES, "phone");
+
+        connect_ports(u, &data, PA_DIRECTION_INPUT);
+
+        if (!u->transport_acquired)
+            switch (u->profile) {
+                case PA_BLUETOOTH_PROFILE_A2DP_SOURCE:
+                case PA_BLUETOOTH_PROFILE_HFP_AG:
+                case PA_BLUETOOTH_PROFILE_HSP_AG:
                     data.suspend_cause = PA_SUSPEND_USER;
-                else
+                    break;
+                case PA_BLUETOOTH_PROFILE_HSP_HS:
+                case PA_BLUETOOTH_PROFILE_HFP_HF:
+                    /* u->stream_fd contains the error returned by the last transport_acquire()
+                     * EAGAIN means we are waiting for a NewConnection signal */
+                    if (u->stream_fd == -EAGAIN)
+                        data.suspend_cause = PA_SUSPEND_USER;
+                    else
+                        pa_assert_not_reached();
+                    break;
+                case PA_BLUETOOTH_PROFILE_A2DP_SINK:
+                case PA_BLUETOOTH_PROFILE_OFF:
                     pa_assert_not_reached();
-                break;
-            case PA_BLUETOOTH_PROFILE_A2DP_SINK:
-            case PA_BLUETOOTH_PROFILE_OFF:
-                pa_assert_not_reached();
-                break;
-        }
+                    break;
+            }
 
-    u->source = pa_source_new(u->core, &data, PA_SOURCE_HARDWARE|PA_SOURCE_LATENCY);
-    pa_source_new_data_done(&data);
-    if (!u->source) {
-        pa_log_error("Failed to create source");
-        return -1;
-    }
+        u->source = pa_source_new(u->core, &data, PA_SOURCE_HARDWARE|PA_SOURCE_LATENCY);
+        pa_source_new_data_done(&data);
+        if (!u->source) {
+            pa_log_error("Failed to create source");
+            return -1;
+        }
 
-    u->source->userdata = u;
-    u->source->parent.process_msg = source_process_msg;
-    u->source->set_state_in_io_thread = source_set_state_in_io_thread_cb;
+        u->source->userdata = u;
+        u->source->parent.process_msg = source_process_msg;
+        u->source->set_state_in_io_thread = source_set_state_in_io_thread_cb;
 
-    source_setup_volume_callback(u->source);
+        source_setup_volume_callback(u->source);
+    }
 
     return 0;
 }
@@ -1250,57 +1308,67 @@ static int add_sink(struct userdata *u) {
 
     pa_assert(u->transport);
 
-    pa_sink_new_data_init(&data);
-    data.module = u->module;
-    data.card = u->card;
-    data.driver = __FILE__;
-    data.name = pa_sprintf_malloc("bluez_sink.%s.%s", u->device->address, pa_bluetooth_profile_to_string(u->profile));
-    data.namereg_fail = false;
-    pa_proplist_sets(data.proplist, "bluetooth.protocol", pa_bluetooth_profile_to_string(u->profile));
-    if (u->bt_codec)
-        pa_proplist_sets(data.proplist, PA_PROP_BLUETOOTH_CODEC, u->bt_codec->name);
-    pa_sink_new_data_set_sample_spec(&data, &u->encoder_sample_spec);
-    if (u->profile == PA_BLUETOOTH_PROFILE_HSP_HS
-        || u->profile == PA_BLUETOOTH_PROFILE_HFP_HF)
-        pa_proplist_sets(data.proplist, PA_PROP_DEVICE_INTENDED_ROLES, "phone");
-
-    connect_ports(u, &data, PA_DIRECTION_OUTPUT);
+    if (USE_SCO_OVER_PCM(u)) {
+        pa_proplist *p;
 
-    if (!u->transport_acquired)
-        switch (u->profile) {
-            case PA_BLUETOOTH_PROFILE_HFP_AG:
-            case PA_BLUETOOTH_PROFILE_HSP_AG:
-                data.suspend_cause = PA_SUSPEND_USER;
-                break;
-            case PA_BLUETOOTH_PROFILE_HSP_HS:
-            case PA_BLUETOOTH_PROFILE_HFP_HF:
-                /* u->stream_fd contains the error returned by the last transport_acquire()
-                 * EAGAIN means we are waiting for a NewConnection signal */
-                if (u->stream_fd == -EAGAIN)
+        u->sink = u->hsp.sco_sink;
+        p = pa_proplist_new();
+        pa_proplist_sets(p, "bluetooth.protocol", pa_bluetooth_profile_to_string(u->profile));
+        pa_proplist_update(u->sink->proplist, PA_UPDATE_MERGE, p);
+        pa_proplist_free(p);
+    } else {
+        pa_sink_new_data_init(&data);
+        data.module = u->module;
+        data.card = u->card;
+        data.driver = __FILE__;
+        data.name = pa_sprintf_malloc("bluez_sink.%s.%s", u->device->address, pa_bluetooth_profile_to_string(u->profile));
+        data.namereg_fail = false;
+        pa_proplist_sets(data.proplist, "bluetooth.protocol", pa_bluetooth_profile_to_string(u->profile));
+        if (u->bt_codec)
+            pa_proplist_sets(data.proplist, PA_PROP_BLUETOOTH_CODEC, u->bt_codec->name);
+        pa_sink_new_data_set_sample_spec(&data, &u->encoder_sample_spec);
+        if (u->profile == PA_BLUETOOTH_PROFILE_HSP_HS
+            || u->profile == PA_BLUETOOTH_PROFILE_HFP_HF)
+            pa_proplist_sets(data.proplist, PA_PROP_DEVICE_INTENDED_ROLES, "phone");
+
+        connect_ports(u, &data, PA_DIRECTION_OUTPUT);
+
+        if (!u->transport_acquired)
+            switch (u->profile) {
+                case PA_BLUETOOTH_PROFILE_HFP_AG:
+                case PA_BLUETOOTH_PROFILE_HSP_AG:
                     data.suspend_cause = PA_SUSPEND_USER;
-                else
+                    break;
+                case PA_BLUETOOTH_PROFILE_HSP_HS:
+                case PA_BLUETOOTH_PROFILE_HFP_HF:
+                    /* u->stream_fd contains the error returned by the last transport_acquire()
+                     * EAGAIN means we are waiting for a NewConnection signal */
+                    if (u->stream_fd == -EAGAIN)
+                        data.suspend_cause = PA_SUSPEND_USER;
+                    else
+                        pa_assert_not_reached();
+                    break;
+                case PA_BLUETOOTH_PROFILE_A2DP_SINK:
+                    /* Profile switch should have failed */
+                case PA_BLUETOOTH_PROFILE_A2DP_SOURCE:
+                case PA_BLUETOOTH_PROFILE_OFF:
                     pa_assert_not_reached();
-                break;
-            case PA_BLUETOOTH_PROFILE_A2DP_SINK:
-                /* Profile switch should have failed */
-            case PA_BLUETOOTH_PROFILE_A2DP_SOURCE:
-            case PA_BLUETOOTH_PROFILE_OFF:
-                pa_assert_not_reached();
-                break;
-        }
+                    break;
+            }
 
-    u->sink = pa_sink_new(u->core, &data, PA_SINK_HARDWARE|PA_SINK_LATENCY);
-    pa_sink_new_data_done(&data);
-    if (!u->sink) {
-        pa_log_error("Failed to create sink");
-        return -1;
-    }
+        u->sink = pa_sink_new(u->core, &data, PA_SINK_HARDWARE|PA_SINK_LATENCY);
+        pa_sink_new_data_done(&data);
+        if (!u->sink) {
+            pa_log_error("Failed to create sink");
+            return -1;
+        }
 
-    u->sink->userdata = u;
-    u->sink->parent.process_msg = sink_process_msg;
-    u->sink->set_state_in_io_thread = sink_set_state_in_io_thread_cb;
+        u->sink->userdata = u;
+        u->sink->parent.process_msg = sink_process_msg;
+        u->sink->set_state_in_io_thread = sink_set_state_in_io_thread_cb;
 
-    sink_setup_volume_callback(u->sink);
+        sink_setup_volume_callback(u->sink);
+    }
 
     return 0;
 }
@@ -1322,6 +1390,10 @@ static pa_direction_t get_profile_direction(pa_bluetooth_profile_t p) {
 
 /* Run from main thread */
 static int transport_config(struct userdata *u) {
+
+    pa_log_debug("Configuring transport for profile %s",
+                 pa_bluetooth_profile_to_string(u->profile));
+
     pa_assert(u);
     pa_assert(u->transport);
     pa_assert(!u->bt_codec);
@@ -1361,11 +1433,18 @@ static int setup_transport(struct userdata *u) {
     pa_bluetooth_transport *t;
 
     pa_assert(u);
-    pa_assert(!u->transport);
+    pa_assert(!u->transport_acquired);
     pa_assert(u->profile != PA_BLUETOOTH_PROFILE_OFF);
 
+    pa_log_debug("profile %s", pa_bluetooth_profile_to_string(u->profile));
+
     /* check if profile has a transport */
     t = u->device->transports[u->profile];
+
+    pa_log_debug("profile %s transport %p transport state %s",
+                 pa_bluetooth_profile_to_string(u->profile),
+                 t, t ? pa_bluetooth_transport_state_to_string(t->state) : "unknown");
+
     if (!t || t->state <= PA_BLUETOOTH_TRANSPORT_STATE_DISCONNECTED) {
         pa_log_warn("Profile %s has no transport", pa_bluetooth_profile_to_string(u->profile));
         return -1;
@@ -1392,6 +1471,8 @@ static int init_profile(struct userdata *u) {
     pa_assert(u);
     pa_assert(u->profile != PA_BLUETOOTH_PROFILE_OFF);
 
+    pa_log_debug("Initializing profile %s", pa_bluetooth_profile_to_string(u->profile));
+
     r = setup_transport(u);
     if (r == -EINPROGRESS)
         return 0;
@@ -1400,6 +1481,9 @@ static int init_profile(struct userdata *u) {
 
     pa_assert(u->transport);
 
+    pa_log_debug("Transport for profile %s successfully setup",
+                 pa_bluetooth_profile_to_string(u->profile));
+
     if (get_profile_direction (u->profile) & PA_DIRECTION_OUTPUT)
         if (add_sink(u) < 0)
             r = -1;
@@ -1714,6 +1798,67 @@ finish:
     pa_log_debug("IO thread shutting down");
 }
 
+static int sco_over_pcm_state_update(struct userdata *u, bool changed)
+{
+    pa_assert(u);
+    pa_assert(USE_SCO_OVER_PCM(u));
+
+    pa_log_debug("Updating SCO over PCM state (profile %s, changed %s, stream fd %d)",
+                 pa_bluetooth_profile_to_string(u->profile),
+                 changed ? "yes" : "no", u->stream_fd);
+
+    if (PA_SINK_IS_OPENED(u->hsp.sco_sink->state) ||
+        PA_SOURCE_IS_OPENED(u->hsp.sco_source->state)) {
+
+        if (u->stream_fd >= 0)
+            return 0;
+
+        pa_log_debug("Resuming SCO over PCM");
+
+        if (transport_acquire(u, false) < 0) {
+            pa_log("Can't resume SCO over PCM");
+            return -1;
+        }
+
+        setup_stream(u);
+
+        return 0;
+    }
+
+    if (changed) {
+        if (u->stream_fd < 0)
+            return 0;
+
+        if (check_proplist(u) == 1) {
+            pa_log_debug("Suspend prevention active, not closing SCO over PCM");
+            return 0;
+        }
+
+        pa_log_debug("Closing SCO over PCM");
+
+        transport_release(u);
+    }
+
+    return 0;
+}
+
+static void stream_died_cb(pa_mainloop_api *ea, pa_io_event *e, int fd, pa_io_event_flags_t events, void *userdata)
+{
+    struct userdata *u = userdata;
+
+    pa_assert(u);
+    pa_assert(u->transport);
+
+    pa_log_warn("SCO stream went down");
+
+    pa_bluetooth_transport_set_state(u->transport, PA_BLUETOOTH_TRANSPORT_STATE_IDLE);
+
+    /* Don't fire this callback again. */
+    pa_assert(u->stream_event);
+    u->core->mainloop->io_free(u->stream_event);
+    u->stream_event = NULL;
+}
+
 /* Run from main thread */
 static int start_thread(struct userdata *u) {
     pa_assert(u);
@@ -1728,6 +1873,25 @@ static int start_thread(struct userdata *u) {
         return -1;
     }
 
+    if (USE_SCO_OVER_PCM(u)) {
+        if (sco_over_pcm_state_update(u, false) < 0)
+            return -1;
+
+        pa_log_debug("Installing monitor for SCO stream");
+
+        u->stream_event = u->core->mainloop->io_new(u->core->mainloop,
+                            u->stream_fd, PA_IO_EVENT_HANGUP | PA_IO_EVENT_ERROR, stream_died_cb, u);
+        if (!u->stream_event) {
+            pa_log_error("Failed to setup monitoring for SCO stream");
+            return -1;
+        }
+
+        pa_sink_ref(u->sink);
+        pa_source_ref(u->source);
+
+        return 0;
+    }
+
     if (!(u->thread = pa_thread_new("bluetooth", thread_func, u))) {
         pa_log_error("Failed to create IO thread");
         return -1;
@@ -1794,10 +1958,10 @@ static void stop_thread(struct userdata *u) {
     if (u->sink || u->source)
         pa_proplist_unset(u->card->proplist, PA_PROP_BLUETOOTH_CODEC);
 
-    if (u->sink)
+    if (u->sink && !USE_SCO_OVER_PCM(u))
         pa_sink_unlink(u->sink);
 
-    if (u->source)
+    if (u->source && !USE_SCO_OVER_PCM(u))
         pa_source_unlink(u->source);
 
     if (u->thread) {
@@ -2134,6 +2298,22 @@ static pa_card_profile *create_card_profile(struct userdata *u, pa_bluetooth_pro
     return cp;
 }
 
+static void save_sco_volume_callbacks(struct userdata *u) {
+    pa_assert(u);
+    pa_assert(USE_SCO_OVER_PCM(u));
+
+    u->hsp.sco_sink_set_volume = u->hsp.sco_sink->set_volume;
+    u->hsp.sco_source_set_volume = u->hsp.sco_source->set_volume;
+}
+
+static void restore_sco_volume_callbacks(struct userdata *u) {
+    pa_assert(u);
+    pa_assert(USE_SCO_OVER_PCM(u));
+
+    pa_sink_set_set_volume_callback(u->hsp.sco_sink, u->hsp.sco_sink_set_volume);
+    pa_source_set_set_volume_callback(u->hsp.sco_source, u->hsp.sco_source_set_volume);
+}
+
 /* Run from main thread */
 static int set_profile_cb(pa_card *c, pa_card_profile *new_profile) {
     struct userdata *u;
@@ -2145,6 +2325,10 @@ static int set_profile_cb(pa_card *c, pa_card_profile *new_profile) {
 
     p = PA_CARD_PROFILE_DATA(new_profile);
 
+    pa_log_debug("Setting new profile %s for card (current %s)",
+                 pa_bluetooth_profile_to_string(*p),
+                 pa_bluetooth_profile_to_string(u->profile));
+
     if (*p != PA_BLUETOOTH_PROFILE_OFF) {
         const pa_bluetooth_device *d = u->device;
 
@@ -2287,6 +2471,12 @@ static int add_card(struct userdata *u) {
     p = PA_CARD_PROFILE_DATA(u->card->active_profile);
     u->profile = *p;
 
+    if (USE_SCO_OVER_PCM(u))
+        save_sco_volume_callbacks(u);
+
+    pa_log_debug("Created card (current profile %s)",
+                 pa_bluetooth_profile_to_string(u->profile));
+
     return 0;
 }
 
@@ -2296,13 +2486,15 @@ static void handle_transport_state_change(struct userdata *u, struct pa_bluetoot
     bool release = false;
     pa_card_profile *cp;
     pa_device_port *port;
-    pa_available_t oldavail;
 
     pa_assert(u);
     pa_assert(t);
     pa_assert_se(cp = pa_hashmap_get(u->card->profiles, pa_bluetooth_profile_to_string(t->profile)));
 
-    oldavail = cp->available;
+    pa_log_debug("State of transport for profile %s changed to %s",
+                 pa_bluetooth_profile_to_string(t->profile),
+                 pa_bluetooth_transport_state_to_string(t->state));
+
     /*
      * If codec switching is in progress, transport state change should not
      * make profile unavailable.
@@ -2318,9 +2510,13 @@ static void handle_transport_state_change(struct userdata *u, struct pa_bluetoot
 
     /* Acquire or release transport as needed */
     acquire = (t->state == PA_BLUETOOTH_TRANSPORT_STATE_PLAYING && u->profile == t->profile);
-    release = (oldavail != PA_AVAILABLE_NO && t->state != PA_BLUETOOTH_TRANSPORT_STATE_PLAYING && u->profile == t->profile);
+    release = (t->state != PA_BLUETOOTH_TRANSPORT_STATE_PLAYING && u->profile == t->profile);
 
     if (acquire && transport_acquire(u, true) >= 0) {
+
+        pa_log_debug("Acquiring transport for profile %s",
+                     pa_bluetooth_profile_to_string(t->profile));
+
         if (u->source) {
             pa_log_debug("Resuming source %s because its transport state changed to playing", u->source->name);
 
@@ -2360,6 +2556,9 @@ static void handle_transport_state_change(struct userdata *u, struct pa_bluetoot
          * BlueZ should probably release the transport automatically, and in
          * that case we would just mark the transport as released */
 
+        pa_log_debug("Releasing transport for profile %s",
+                     pa_bluetooth_profile_to_string(t->profile));
+
         /* Remote side closed the stream so we consider it PA_SUSPEND_USER */
         if (u->source) {
             pa_log_debug("Suspending source %s because the remote end closed the stream", u->source->name);
@@ -2411,6 +2610,10 @@ static pa_hook_result_t transport_state_changed_cb(pa_bluetooth_discovery *y, pa
     pa_assert(t);
     pa_assert(u);
 
+    pa_log_debug("State of transport for profile %s has changed to %s",
+                 pa_bluetooth_profile_to_string(t->profile),
+                 pa_bluetooth_transport_state_to_string(t->state));
+
     if (t == u->transport && t->state <= PA_BLUETOOTH_TRANSPORT_STATE_DISCONNECTED)
         pa_assert_se(pa_card_set_profile(u->card, pa_hashmap_get(u->card->profiles, "off"), false) >= 0);
 
@@ -2437,7 +2640,8 @@ static pa_hook_result_t transport_sink_volume_changed_cb(pa_bluetooth_discovery
         return PA_HOOK_OK;
     }
 
-    sink_setup_volume_callback(u->sink);
+    if (!USE_SCO_OVER_PCM(u))
+        sink_setup_volume_callback(u->sink);
 
     pa_cvolume_set(&v, u->encoder_sample_spec.channels, volume);
     if (pa_bluetooth_profile_should_attenuate_volume(t->profile))
@@ -2465,7 +2669,8 @@ static pa_hook_result_t transport_source_volume_changed_cb(pa_bluetooth_discover
         return PA_HOOK_OK;
     }
 
-    source_setup_volume_callback(u->source);
+    if (!USE_SCO_OVER_PCM(u))
+        source_setup_volume_callback(u->source);
 
     pa_cvolume_set(&v, u->decoder_sample_spec.channels, volume);
 
@@ -2477,6 +2682,36 @@ static pa_hook_result_t transport_source_volume_changed_cb(pa_bluetooth_discover
     return PA_HOOK_OK;
 }
 
+static pa_hook_result_t sink_state_changed_cb(pa_core *c, pa_sink *s, struct userdata *u) {
+    pa_assert(c);
+    pa_sink_assert_ref(s);
+    pa_assert(u);
+
+    pa_log_debug("Sink %s state has changed", s->name);
+
+    if (!USE_SCO_OVER_PCM(u) || s != u->hsp.sco_sink)
+        return PA_HOOK_OK;
+
+    sco_over_pcm_state_update(u, true);
+
+    return PA_HOOK_OK;
+}
+
+static pa_hook_result_t source_state_changed_cb(pa_core *c, pa_source *s, struct userdata *u) {
+    pa_assert(c);
+    pa_source_assert_ref(s);
+    pa_assert(u);
+
+    pa_log_debug("Source %s state has changed", s->name);
+
+    if (!USE_SCO_OVER_PCM(u) || s != u->hsp.sco_source)
+        return PA_HOOK_OK;
+
+    sco_over_pcm_state_update(u, true);
+
+    return PA_HOOK_OK;
+}
+
 static char* make_message_handler_path(const char *name) {
     return pa_sprintf_malloc("/card/%s/bluez", name);
 }
@@ -2760,6 +2995,18 @@ int pa__init(pa_module* m) {
         goto fail_free_modargs;
     }
 
+      if (pa_modargs_get_value(ma, "sco_sink", NULL) &&
+        !(u->hsp.sco_sink = pa_namereg_get(m->core, pa_modargs_get_value(ma, "sco_sink", NULL), PA_NAMEREG_SINK))) {
+        pa_log("SCO sink not found");
+        goto fail_free_modargs;
+    }
+
+    if (pa_modargs_get_value(ma, "sco_source", NULL) &&
+        !(u->hsp.sco_source = pa_namereg_get(m->core, pa_modargs_get_value(ma, "sco_source", NULL), PA_NAMEREG_SOURCE))) {
+        pa_log("SCO source not found");
+        goto fail_free_modargs;
+    }
+
     if ((u->discovery = pa_shared_get(u->core, "bluetooth-discovery")))
         pa_bluetooth_discovery_ref(u->discovery);
     else {
@@ -2816,6 +3063,14 @@ int pa__init(pa_module* m) {
     u->transport_source_volume_changed_slot =
         pa_hook_connect(pa_bluetooth_discovery_hook(u->discovery, PA_BLUETOOTH_HOOK_TRANSPORT_SOURCE_VOLUME_CHANGED), PA_HOOK_NORMAL, (pa_hook_cb_t) transport_source_volume_changed_cb, u);
 
+    u->sink_state_changed_slot =
+        pa_hook_connect(&u->core->hooks[PA_CORE_HOOK_SINK_STATE_CHANGED],
+                        PA_HOOK_NORMAL, (pa_hook_cb_t) sink_state_changed_cb, u);
+
+    u->source_state_changed_slot =
+        pa_hook_connect(&u->core->hooks[PA_CORE_HOOK_SOURCE_STATE_CHANGED],
+                        PA_HOOK_NORMAL, (pa_hook_cb_t) source_state_changed_cb, u);
+
     if (add_card(u) < 0)
         goto fail;
 
@@ -2894,12 +3149,21 @@ void pa__done(pa_module *m) {
     if (u->transport_source_volume_changed_slot)
         pa_hook_slot_free(u->transport_source_volume_changed_slot);
 
+    if (u->sink_state_changed_slot)
+        pa_hook_slot_free(u->sink_state_changed_slot);
+
+    if (u->source_state_changed_slot)
+        pa_hook_slot_free(u->source_state_changed_slot);
+
     if (u->encoder_buffer)
         pa_xfree(u->encoder_buffer);
 
     if (u->decoder_buffer)
         pa_xfree(u->decoder_buffer);
 
+    if (USE_SCO_OVER_PCM(u))
+        restore_sco_volume_callbacks(u);
+
     if (u->msg)
         pa_xfree(u->msg);
 
diff --git a/src/modules/bluetooth/module-bluez5-discover.c b/src/modules/bluetooth/module-bluez5-discover.c
index b13517e..3771489 100644
--- a/src/modules/bluetooth/module-bluez5-discover.c
+++ b/src/modules/bluetooth/module-bluez5-discover.c
@@ -42,6 +42,8 @@ PA_MODULE_USAGE(
     "enable_native_hsp_hs=<boolean, enable HSP support in native backend>"
     "enable_native_hfp_hf=<boolean, enable HFP support in native backend>"
     "avrcp_absolute_volume=<synchronize volume with peer, true by default>"
+    "sco_sink=<name of sink> "
+    "sco_source=<name of source> "
 );
 
 static const char* const valid_modargs[] = {
@@ -52,6 +54,8 @@ static const char* const valid_modargs[] = {
     "enable_native_hsp_hs",
     "enable_native_hfp_hf",
     "avrcp_absolute_volume",
+    "sco_sink",
+    "sco_source",
     NULL
 };
 
@@ -64,6 +68,8 @@ struct userdata {
     bool autodetect_mtu;
     bool avrcp_absolute_volume;
     uint32_t output_rate_refresh_interval_ms;
+    char *sco_sink;
+    char *sco_source;
 };
 
 static pa_hook_result_t device_connection_changed_cb(pa_bluetooth_discovery *y, const pa_bluetooth_device *d, struct userdata *u) {
@@ -93,6 +99,15 @@ static pa_hook_result_t device_connection_changed_cb(pa_bluetooth_discovery *y,
                                        u->output_rate_refresh_interval_ms,
                                        (int)u->avrcp_absolute_volume);
 
+        if (u->sco_sink && u->sco_source) {
+            char *tmp;
+
+            tmp = pa_sprintf_malloc("%s sco_sink=\"%s\" sco_source=\"%s\"", args,
+                                    u->sco_sink, u->sco_source);
+            pa_xfree(args);
+            args = tmp;
+        }
+
         pa_log_debug("Loading module-bluez5-device %s", args);
         pa_module_load(&m, u->module->core, "module-bluez5-device", args);
         pa_xfree(args);
@@ -188,6 +203,8 @@ int pa__init(pa_module *m) {
     u->avrcp_absolute_volume = avrcp_absolute_volume;
     u->output_rate_refresh_interval_ms = output_rate_refresh_interval_ms;
     u->loaded_device_paths = pa_hashmap_new(pa_idxset_string_hash_func, pa_idxset_string_compare_func);
+    u->sco_sink = pa_xstrdup(pa_modargs_get_value(ma, "sco_sink", NULL));
+    u->sco_source = pa_xstrdup(pa_modargs_get_value(ma, "sco_source", NULL));
 
     if (!(u->discovery = pa_bluetooth_discovery_get(u->core, headset_backend, enable_native_hsp_hs, enable_native_hfp_hf, enable_msbc)))
         goto fail;
@@ -220,6 +237,12 @@ void pa__done(pa_module *m) {
     if (u->loaded_device_paths)
         pa_hashmap_free(u->loaded_device_paths);
 
+    if (u->sco_sink)
+        pa_xfree(u->sco_sink);
+
+    if (u->sco_source)
+        pa_xfree(u->sco_source);
+
     if (u->discovery)
         pa_bluetooth_discovery_unref(u->discovery);
 
